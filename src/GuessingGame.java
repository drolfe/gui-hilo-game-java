import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/*
 * Created by JFormDesigner on Fri Jan 26 15:08:25 PST 2018
 */



/**
 * @author Danniel Rolfe
 */
public class GuessingGame extends JFrame {
    private  int theNumber;
    private int attempts;
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Danniel Rolfe
    private JLabel title;
    private JPanel content;
    private JLabel lblInput;
    private JTextField txtGuess;
    private JButton btnGuess;
    private JLabel lblOutput;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    public GuessingGame() {
        initComponents();
    }

    public void newGame() {
        theNumber = (int)(Math.random() * 100 + 1);
    }

    public void checkGuess() {
        String guessText = txtGuess.getText();
        String message = "";

        try {

            int guess = Integer.parseInt(guessText);

            if (attempts == 7) {
                message = "You have reached the max attempts: " + attempts;
                newGame();
                return;
            }

            if (guess > theNumber) {
                attempts++;
                message = guess + " was to high. You have attempted " + attempts + " times. Guess again!";
            } else if (guess < theNumber) {
                attempts++;
                message = guess + " was to low. You have attempted " + attempts + " times. Guess again!";
            } else {
                message = guess + " is correct. Play again!";
                newGame();
            }
        } catch (Exception e) {
            message = "Enter a whole number between 1 and 100.";
            lblOutput.setText(message);
        }

        finally {
            lblOutput.setText(message);
            txtGuess.requestFocus();
            txtGuess.selectAll();
        }
    }

    private void btnGuessActionPerformed(ActionEvent e) {
        // TODO add your code here
        checkGuess();
    }

    private void txtGuessActionPerformed(ActionEvent e) {
        // TODO add your code here
        checkGuess();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Danniel Rolfe
        title = new JLabel();
        content = new JPanel();
        lblInput = new JLabel();
        txtGuess = new JTextField();
        btnGuess = new JButton();
        lblOutput = new JLabel();

        //======== this ========
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //---- title ----
        title.setText("Guessing Game by Danniel Rolfe");
        title.setHorizontalAlignment(SwingConstants.CENTER);
        title.setFont(new Font("Aktiv Grotesk", Font.BOLD | Font.ITALIC, 14));
        contentPane.add(title);
        title.setBounds(0, 0, 400, 75);

        //======== content ========
        {

            // JFormDesigner evaluation mark
            content.setBorder(new javax.swing.border.CompoundBorder(
                new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                    "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                    javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                    java.awt.Color.red), content.getBorder())); content.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

            content.setLayout(null);

            //---- lblInput ----
            lblInput.setText("Guess a number between 1 and 100:");
            lblInput.setHorizontalAlignment(SwingConstants.RIGHT);
            content.add(lblInput);
            lblInput.setBounds(25, 20, 234, 25);

            //---- txtGuess ----
            txtGuess.addActionListener(e -> txtGuessActionPerformed(e));
            content.add(txtGuess);
            txtGuess.setBounds(270, 20, 95, 25);

            //---- btnGuess ----
            btnGuess.setText("Guess");
            btnGuess.addActionListener(e -> btnGuessActionPerformed(e));
            content.add(btnGuess);
            btnGuess.setBounds(145, 65, 115, 40);

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < content.getComponentCount(); i++) {
                    Rectangle bounds = content.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = content.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                content.setMinimumSize(preferredSize);
                content.setPreferredSize(preferredSize);
            }
        }
        contentPane.add(content);
        content.setBounds(0, 75, 400, 125);

        //---- lblOutput ----
        lblOutput.setText("Enter a number above and click guess.");
        lblOutput.setHorizontalAlignment(SwingConstants.CENTER);
        contentPane.add(lblOutput);
        lblOutput.setBounds(0, 235, 400, 15);

        { // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    public static void main(String[] args) {
        GuessingGame theGame = new GuessingGame();
        theGame.newGame();
        theGame.setSize(new Dimension(450, 350));
        theGame.setVisible(true);
    }

}
